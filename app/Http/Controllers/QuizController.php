<?php

namespace App\Http\Controllers;

use App\Models\Answer;
use App\Models\Question;
use App\Models\Serie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class QuizController extends Controller
{

    private $answer;

    private $question;

    private $serie;

    public function __construct(Answer $answer, Question $question, Serie $serie)
    {
        $this->answer = $answer;
        $this->question = $question;
        $this->serie = $serie;
    }

    /**
        MÉTODO UTILIZADO PARA INICIAR O DEFAFIO REENDERIZANDO A VIEW
     */
    public function index()
    {
        $this->clearSession();
        return view('quiz.index');
    }

    /**
        MÉTODO UTILIZADO PARA RENDERIZAR A PAGE DO QUESTIONÁRIO
     */
    public function questionario()
    {
        return view('quiz.questionario');
    }

    /**
        MÉTODO UTILIZADO PARA REENDERIZANDO A VIEW DE FINALIZAÇÃO DO DESAFIO
     */
    public function finalizar()
    {
        $session = app('session.store');
        $list_answer = $session->get('quiz.answered');
        $this->saveYourSerie($list_answer);
        return view('quiz.finalizar',['player' => $session->get('jogador.nome')]);
    }

    /**
        MÉTODO UTILIZADO PARA BUSCAR A PERGUNTA QUE VAI SER RESPONDIDA PELO JOGADOR
     */
    public function getQuestion()
    {
        $session = app('session.store');
        $finish = true;

        if ($session->has('quiz.question')) {
            $answeredQuestions = $session->get('quiz.question');
            $questions = $this->question->getQuestions($answeredQuestions);
        } else {
            $answeredQuestions = [0];
            $questions = $this->question->getQuestions($answeredQuestions);
        }

        if (count($questions) == 0) {
            return Response::json(['success' => true, 'finish' => $finish]);
        }

        $finish = false;
        $first = $questions->first();
        $last = $questions->last();
        $question_id = rand($first->id, $last->id);

        while (in_array($question_id, $answeredQuestions)) {
            $question_id = rand($first->id, $last->id);
        }

        if (!$session->has('quiz.question')) {
            $session->put('quiz.question', array($question_id));
            $session->save();
        } else {
            $question = $this->question->find($question_id);
            $answers = $question->answers;
            $session->push('quiz.question', $question_id);
        }

        return Response::json(['success' => true, 'question' => $question->question, 'answers' => $answers, 'finish' => $finish]);
    }

    /**
        MÉTODO UTILIZADO PARA SALVAR O NOME DO JOGADOR NA SESSÃO
     */
    public function saveNameSession(Request $request)
    {
        $session = app('session.store');
        $bool = false;
        $name = [];

        if ($request->has('name')) {
            $bool = true;
            $name = $request->get('name');
        }

        $session->put('jogador.nome', $name);
        $session->save();

        return Response::json(['success' => $bool, 'name' => $name]);
    }

    /**
        MÉTODO UTILIZADO PARA SALVAR AS RESPOSTAS DO JOGADOR NA SESSÃO
     */
    public function saveAnswer(Request $request)
    {
        $session = app('session.store');
        $question = $request->get('question_id');

        if ($session->has('quiz.answered')) {
            $session->push('quiz.answered', $question);
        } else {
            $session->put('quiz.answered', array($question));
            $session->save();
        }

        return Response::json(['success' => true]);
    }

    /**
        MÉTODO UTILIZADO PARA LIMPAR OS DADOS SALVOS NA SESSÃO
     */
    public function clearSession()
    {
        $session = app('session.store');
        $session->put('jogador.nome', []);
        $session->put('quiz.question', []);
        $session->put('quiz.answered', []);
        $session->put('quiz.result', []);
        $session->save();
    }

    /**
        MÉTODO UTILIZADO PARA DESCOBRIR E SALVAR SUA SERIE NA SESSION
     */
    public function saveYourSerie($list)
    {
        $session = app('session.store');

        $answers = $this->answer->getAnswers($list);
        $houseofcards = 0;
        $gameofthrones = 0;
        $lost = 0;
        $breakingbad = 0;
        $silicon = 0;
        $absoluteValue = 0;
        $message = '';

        // INICIO DE VALIDAÇÃO POR QUANTIDADE
        foreach ($answers AS $answer) {
            if ($answer->serie_id == 1)
                $houseofcards++;
            if ($answer->serie_id == 2)
                $gameofthrones++;
            if ($answer->serie_id == 3)
                $lost++;
            if ($answer->serie_id == 4)
                $breakingbad++;
            if ($answer->serie_id == 5)
                $silicon++;
        }

        if ($gameofthrones > 2){
            $session->put('quiz.result', Serie::GAME_OF_THRONES);
            $session->save();
            return true;
        }
        if ($lost > 2) {
            $session->put('quiz.result', Serie::LOST);
            $session->save();
            return true;
        }
        if ($houseofcards > 2) {
            $session->put('quiz.result', Serie::HOUSE_OF_CARDS);
            $session->save();
            return true;
        }
        if ($breakingbad > 2) {
            $session->put('quiz.result', Serie::BREAKING_BAD);
            $session->save();
            return true;
        }
        if ($silicon > 2) {
            $session->put('quiz.result', Serie::SILICON_VALLEY);
            $session->save();
            return true;
        }

        $houseofcards = 0;
        $gameofthrones = 0;
        $lost = 0;
        $breakingbad = 0;
        $silicon = 0;

        // FIM DE VALIDAÇÃO POR QUANTIDADE
        // INICIO VALIDAÇÃO POR PESO

        foreach ($answers AS $answer) {
            if ($answer->serie_id == 1)
                $houseofcards =  $houseofcards + $answer->question->weight;
            if ($answer->serie_id == 2)
                $gameofthrones = $gameofthrones + $answer->question->weight;
            if ($answer->serie_id == 3)
                $lost = $lost + $answer->question->weight;
            if ($answer->serie_id == 4)
                $breakingbad = $breakingbad + $answer->question->weight;
            if ($answer->serie_id == 5)
                $silicon = $silicon + $answer->question->weight;
        }

        $result = [
            ['message' => Serie::SILICON_VALLEY ,'value' => $silicon],
            ['message' => Serie::GAME_OF_THRONES ,'value' => $gameofthrones],
            ['message' => Serie::LOST ,'value' => $lost],
            ['message' => Serie::BREAKING_BAD ,'value' => $breakingbad],
            ['message' => Serie::HOUSE_OF_CARDS ,'value' => $houseofcards],
        ];

        for ($row = 0; $row < count($result); $row++) {
           if($result[$row]['value'] > $absoluteValue) {
               $absoluteValue = $result[$row]['value'];
               $message = $result[$row]['message'];
           }
        }

        $session->put('quiz.result', $message);
        $session->save();
        return true;

        // FIM VALIDAÇÃO POR PESO
    }

    /**
        MÉTODO UTILIZADO PARA PEGAR A SERIE DA EQUIVALENTE AO JOGADOR
     */
    public function getYousSerie(){
        $session = app('session.store');
        $msg = '';

        if($session->has('quiz.result'))
            $msg = $session->get('quiz.result');

        return Response::json(['success' => true, 'message' => $msg]);
    }
}
