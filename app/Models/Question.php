<?php

/**
 * Created by PhpStorm.
 * User: Ezequiel
 * Date: 15/08/2017
 * Time: 13:03
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{

    public $table = 'questions';

    /**
     * Variaveis seguras para uso e guardar dados
     * @var array
     */
    public $fillable = [
        'id',
        'question',
        'weight',
        'serie_id',
    ];


    /**
        M�TODO QUE RETORNA UMA COLE��O DE DADOS ONDE N�O FASSAM PARTE DA LISTA AO QUAL JA FOI RESPONDIDA PELO JOGADOR.
     */
    public function getQuestions($answeredQuestions){
        return self::whereNotIn('id',$answeredQuestions)->get();
    }


    /**
        M�TODO QUE FAZ A RELATION DO ELOUQUENT PARA RETORNAR AS ALTERNATIVAS ASSOCIADAS A QUEST�O.
     */
    public function answers() {
        return $this->hasMany('App\Models\Answer', 'question_id', 'id');
    }
}