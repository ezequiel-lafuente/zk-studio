<?php

/**
 * Created by PhpStorm.
 * User: Ezequiel
 * Date: 15/08/2017
 * Time: 13:03
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{

    public $table = 'answers';

    /**
     * Variaveis seguras para uso e guardar dados
     * @var array
     */
    public $fillable = [
        'id',
        'answer',
        'question_id',
    ];

    /**
        M�TODO QUE RETORNA UMA COLE��O DE ALTERNATIVAS ONDE ATENDAM A LISTA PASSADA COMO PAR�METRO
     */

    public function getAnswers($list){
        return self::whereIn('id', $list)->get();
    }

    /**
        M�TODO QUE FAZ A RELATION DO ELOUQUENT PARA RETORNAR A SERIE QUE ESTA ASSOCIADA A ALTERNATIVA
     */
    public function serie(){
        return $this->hasOne('App\Models\Serie', 'id', 'serie_id');
    }

    /**
    M�TODO QUE FAZ A RELATION DO ELOUQUENT PARA RETORNAR A QUEST�O QUE ESTA ASSOCIADA A ALTERNATIVA
     */
    public function question(){
        return $this->hasOne('App\Models\Question', 'id', 'question_id');
    }
}