<?php

/**
 * Created by PhpStorm.
 * User: Ezequiel
 * Date: 15/08/2017
 * Time: 13:03
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Serie extends Model
{

    const HOUSE_OF_CARDS = "Você é House of Cards, ataca o problema com método e faz de tudo para resolver a situação.";
    const GAME_OF_THRONES = "Você é Game of Thrones, não tem muita delicadeza nas ações, mas resolve o problema de forma prática.";
    const LOST = "Você é Lost, faz as coisas sem ter total certeza se é o caminho certo ou se faz sentido, mas no final dá tudo certo.";
    const BREAKING_BAD = "Você é Breaking Bad, pra fazer acontecer você toma a liderança, mas sempre contando com seus parceiros.";
    const SILICON_VALLEY = "Você é Silicon Valley, vive a tecnologia o tempo todo e faz disso um mantra para cada situação no dia.";

    public $table = 'series';

    /**
     * Variaveis seguras para uso e guardar dados
     * @var array
     */
    public $fillable = [
        'id',
        'estado_id',
        'nome',
    ];


}