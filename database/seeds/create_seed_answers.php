<?php

use Illuminate\Database\Seeder;

class create_seed_answers extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Acorda cedo e come frutas cortadas metodicamente.',
            'question_id' => 1,
            'serie_id' => 1,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Sai da cama com o despertador e se prepara para a batalha da semana.',
            'question_id' => 1,
            'serie_id' => 2,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Só consegue lembrar do seu nome depois do café.',
            'question_id' => 1,
            'serie_id' => 3,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Levanta e faz café pra todos da casa.',
            'question_id' => 1,
            'serie_id' => 4,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Passa o café e conserta um erro no HTML.',
            'question_id' => 1,
            'serie_id' => 5,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Ela vai atrapalhar seu horário. Oculte o corpo.',
            'question_id' => 2,
            'serie_id' => 1,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Levanta a senhora e jura protegê-la com sua vida.',
            'question_id' => 2,
            'serie_id' => 2,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Ajuda-a, mas questiona sua real identidade.',
            'question_id' => 2,
            'serie_id' => 3,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Oferece para caminharem juntos até um destino em comum.',
            'question_id' => 2,
            'serie_id' => 4,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Testa se ela roda bem no Firefox. Não roda.',
            'question_id' => 2,
            'serie_id' => 5,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Convence parte das pessoas a esperarem o próximo.',
            'question_id' => 3,
            'serie_id' => 1,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Ignora as pessoas no elevador e entra de qualquer forma.',
            'question_id' => 3,
            'serie_id' => 2,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Você questiona a realidade, as coisas e tudo mais. Sobe de escada.',
            'question_id' => 3,
            'serie_id' => 3,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Com uma leve intimidação passivo-agressiva, encontra um lugar no elevador.',
            'question_id' => 3,
            'serie_id' => 4,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Cria um app que mostra a lotação do elevador. Vende o app e fica milionário',
            'question_id' => 3,
            'serie_id' => 5,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Fala sobre a política, eleições, como tudo é um absurdo.',
            'question_id' => 4,
            'serie_id' => 1,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Larga uma frase polêmica e vê uma pequena guerra se formar.',
            'question_id' => 4,
            'serie_id' => 2,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Puxa um assunto e te lembram que já foi discutido semana passada.',
            'question_id' => 4,
            'serie_id' => 3,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Sugere que os colegas trabalhem na ideia de um novo projeto.',
            'question_id' => 4,
            'serie_id' => 4,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Desabafa sobre como odeia PHP. Todo mundo na sala adora PHP.',
            'question_id' => 4,
            'serie_id' => 5,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Vou chamar aqui o meu Uber.',
            'question_id' => 5,
            'serie_id' => 1,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Pegarei o bus junto com o resto do povo.',
            'question_id' => 5,
            'serie_id' => 2,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'No ponto de ônibus mais uma vez, espero não errar a linha de novo.',
            'question_id' => 5,
            'serie_id' => 3,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Vou de carro, mas ofereço uma carona para os colegas.',
            'question_id' => 5,
            'serie_id' => 4,
        ));
        \Illuminate\Support\Facades\DB::table('answers')->insert(array(
            'answer' => 'Acho que descobri uma forma de fazer aquela senhora rodar no Firefox.',
            'question_id' => 5,
            'serie_id' => 5,
        ));

    }
}
