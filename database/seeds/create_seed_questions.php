<?php

use Illuminate\Database\Seeder;

class create_seed_questions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('questions')->insert(array(
            'question' => 'De manhã, você.',
            'weight' => 1,
        ));
        \Illuminate\Support\Facades\DB::table('questions')->insert(array(
            'question' => 'Indo para o trabalho você encontra uma senhora idosa caída na rua.',
            'weight' => 2,
        ));
        \Illuminate\Support\Facades\DB::table('questions')->insert(array(
            'question' => 'Chega no prédio e o elevador está cheio.',
            'weight' => 3,
        ));
        \Illuminate\Support\Facades\DB::table('questions')->insert(array(
            'question' => 'Você chega no trabalho e as convenções sociais te obrigam a puxar assunto.',
            'weight' => 4,
        ));
        \Illuminate\Support\Facades\DB::table('questions')->insert(array(
            'question' => 'A pauta pegou o dia todo, mas você está indo para casa.',
            'weight' => 5,
        ));


    }
}
