<?php

use Illuminate\Database\Seeder;

class create_seed_series extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('series')->insert(array(
            'serie' => 'House of Cards',
        ));
        \Illuminate\Support\Facades\DB::table('series')->insert(array(
            'serie' => 'Game of Thrones',
        ));
        \Illuminate\Support\Facades\DB::table('series')->insert(array(
            'serie' => 'Lost',
        ));
        \Illuminate\Support\Facades\DB::table('series')->insert(array(
            'serie' => 'Breaking Bad',
        ));
        \Illuminate\Support\Facades\DB::table('series')->insert(array(
            'serie' => 'Silicon Valley',
        ));
    }
}
