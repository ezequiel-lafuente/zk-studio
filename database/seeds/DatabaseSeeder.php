<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(create_seed_series::class);
         $this->call(create_seed_questions::class);
         $this->call(create_seed_answers::class);
    }
}
