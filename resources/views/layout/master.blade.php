<!DOCTYPE html>
<html lang="en">
<head>
    @include('layout.head')
</head>

<body>
    <div class="container">
        @yield('conteudo')
    </div>

    @include('layout.footer')
</body>
</html>