@extends('layout.master')

@section('javascript')
    <script src="../../../js/finalizar.js"></script>
@stop

@section('conteudo')
    <div class="jumbotron">
        <h1>Parabéns <span class="user">{{$player}}</span>, já temos o resultado...</h1>
        <p class="msg"></p>
        <p><a class="btn btn-lg btn-primary uncover" role="button">Desvendar :)</a></p>
        <p><a href="{{url('/')}}" class="btn btn-lg btn-success repeat" style="display: none" role="button">Mais uma vez!</a></p>
    </div>
@endsection



