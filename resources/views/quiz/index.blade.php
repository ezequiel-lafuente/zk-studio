@extends('layout.master')

@section('javascript')
    <script src="../../../js/index.js"></script>
@stop

@section('conteudo')
    <div class="jumbotron apresentation" style="display: none">
        <h1>Olá <span class="user"></span>, você está pronto para um desafio ?</h1>
        <p>Você é daqueles que sempre que tem um tempinho sobrando corre para a frente de uma tela?</p>
        <p>Sim?! Então você vai adorar essa brincadeira :)</p>
        <p>Responda as seguintes perguntas, e no final vai saber qual seriado mais se encaixa com você!</p>
        <p>Preparado? então vamos lá :D</p>
        <p><a href="{{url('/questionario')}}" class="btn btn-lg btn-primary" role="button">Começar quiz!</a></p>
    </div>

    <div class="jumbotron define-name" style="display: none">
        <p><a class="btn btn-lg btn-success set-username" role="button">Informar nome</a></p>
    </div>

@endsection

@include('quiz.modal.form')


