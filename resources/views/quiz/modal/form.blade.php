<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Olá, por favor nos informe seu nome?</h4>
            </div>
            <div class="modal-body">
                <div class="">
                    <label>Nome:<span style="color: red"> *</span></label>
                    <input type="text" placeholder="Seu nome" name="nome" class="form-control name">
                    <span class="validate" style="color: red; display: none">Campo obrigatório</span>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closeModal" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-default saveName">Avançar</button>
            </div>
        </div>
    </div>
</div>