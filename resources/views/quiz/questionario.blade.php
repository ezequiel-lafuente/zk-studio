@extends('layout.master')

@section('javascript')
    <script src="../../../js/questionario.js"></script>
@stop

@section('conteudo')
    <div class="jumbotron apresentation">
        <h3 class="title_question"></h3>
        <div class="answer_question"></div>
        <br>
        <p><a class="btn btn-lg btn-success set-option" role="button">Próxima</a></p>
    </div>
@endsection

@include('quiz.modal.form')


