<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'QuizController@index');
Route::get('/questionario', 'QuizController@questionario');
Route::get('/finalizar', 'QuizController@finalizar');


//rotas para ajax

Route::get('ajax/saveNameSession', 'QuizController@saveNameSession');
Route::get('ajax/getQuestion', 'QuizController@getQuestion');
Route::get('ajax/saveAnswer', 'QuizController@saveAnswer');
Route::get('ajax/getYousSerie', 'QuizController@getYousSerie');






