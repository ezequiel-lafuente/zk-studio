jQuery(document).ready(function () {
    jQuery(document).on('click', '.saveName',   saveNameUser);
    jQuery(document).on('click', '.closeModal', function(){
        jQuery('.define-name').show();
    });
    jQuery(document).on('click', '.set-username', function(){
        jQuery('.modal').modal('show');
    });
    jQuery('.modal').modal('show');
});


/**
 M�TODO .JS QUE SALVA O NOME DO USU�RIO
 */
function saveNameUser(){
    var name = jQuery('.name').val();
    if(name != ''){
        jQuery.ajax({
            type: "GET",
            url: '/ajax/saveNameSession',
            dataType: "json",
            data: {
                name: name
            },
            success: function (rs) {
                if (rs.success) {
                    jQuery('.user').text(rs.name);
                    jQuery('.modal').modal('hide');
                    jQuery('.apresentation').show();
                }
            }
        });
    } else {
        jQuery('.validate').show();
    }
}
