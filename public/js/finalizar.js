jQuery(document).ready(function () {
    jQuery(document).on('click', '.uncover', uncoverSerie);
});

/**
 MÉTODO .JS QUE DESVENDA A RESPOSTA E A MOSTRA PARA O JOGADOR
 */
function uncoverSerie() {
    bloqueiaTela();
    jQuery.ajax({
        type: "GET",
        url: "/ajax/getYousSerie",
        dataType: "json",
        success: function (rs) {
            if (rs.success) {
                jQuery('.msg').text(rs.message);
                jQuery('.repeat').show();
                liberaTela();
            }
        }
    });
}
