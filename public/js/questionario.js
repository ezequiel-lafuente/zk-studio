jQuery(document).ready(function () {
    jQuery(document).on('click', '.set-option',   saveOptionSelected);
    seeQuestion();
});


/**
 M�TODO UTILIZADO PARA BUSCAR A PERGUNTA COM SUAS ALTERNATIVAS
 */
function seeQuestion() {
    jQuery.ajax({
        type: "GET",
        url:  "/ajax/getQuestion",
        dataType: "json",
        success: function (rs) {
            if (rs.success) {
                if(!rs.finish) {
                    jQuery('.title_question').text(rs.question);
                    for (i = 0; i < rs.answers.length; i++) {
                        var msg  = '<div class="alternatives"><input type="radio" id="' + rs.answers[i].id + '" value="' + rs.answers[i].id + '" name="item_question"/>';
                            msg += '<span class="question"> ' + rs.answers[i].answer + '</span></div>';
                        jQuery('.answer_question').append(msg);
                    }
                } else {
                    window.location = "/finalizar"
                }
            }
        }
    });
}

/**
 M�TODO UTILIZADO PARA SALVAR A OP��O SELECIONADO PELO JOGADOR NA PERGUNTA
 */
function saveOptionSelected(){
    var option = jQuery("input:checked").val();

    if(option === undefined){
        alert('Espertinho... antes de seguir você precisa selecionar uma alternativa :/');
        return false;
    }

    bloqueiaTela();
    jQuery.ajax({
        type: "GET",
        url: '/ajax/saveAnswer',
        dataType: "json",
        data: {
            question_id : option
        },
        success: function (rs) {
            if (rs.success) {
                seeQuestion();
                jQuery('.title_question').empty();
                jQuery('.question').remove();
                jQuery('input[type=radio]').remove();
                liberaTela();
            }
        }
    });
}